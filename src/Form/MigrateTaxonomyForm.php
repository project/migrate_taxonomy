<?php

namespace Drupal\migrate_taxonomy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;

/**
 * Class MigrateTaxonomyForm.
 */
class MigrateTaxonomyForm extends ConfigFormBase {

  /**
   * Created file entity.
   *
   * @var \Drupal\file\Entity\File
   */
  protected $file;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'migrate_taxonomy.migratetaxonomy',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_taxonomy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('migrate_taxonomy.migratetaxonomy');

    $form = [
      '#attributes' => ['enctype' => 'multipart/form-data'],
    ];
    $form['taxonomy_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Taxonomy vocabulary'),
      '#description' => $this->t('Select taxonomy vocabulary'),
      '#options' => $this->getVocabulary(),
      '#size' => 5,
      '#default_value' => $config->get('taxonomy_vocabulary'),
      '#required' => TRUE,
    ];
    $form['taxonomy_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Taxonomy File'),
      '#description' => $this->t('Uploadd taxonomy file.'),
      '#upload_location' => 'public://migrate_file/',
      '#default_value' => $config->get('taxonomy_file'),
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
        'file_validate_size' => [25600000],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);
    $taxonomy_vocabulary = $form_state->getValue('taxonomy_vocabulary');
    /* Fetch the array of the file stored temporarily in database */
    $csv_file = $form_state->getValue('taxonomy_file');
    /* Load the object of the file by it's fid */
    $file = File::load($csv_file[0]);
    /* Set the status flag permanent of the file object */
    $file->setPermanent();
    /* Save the file in database */
    $file->save();

    $taxonomy_term = $this->csvtoarray($file->getFileUri(), ',');
    foreach ($taxonomy_term as $term) {
      if ($term['vid'] !== $taxonomy_vocabulary) {
        \Drupal::messenger()->addMessage($this->t('Please check selected vocabulary or uploaded csv file.'), 'error');
        return FALSE;
      }

      $term_data = $this->cleanTerm($term);
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('vid', $taxonomy_vocabulary);
      $query->condition('name', $term_data['name']);
      $tids = $query->execute();
      if (empty($tids)) {
        Term::create($term_data)->enforceIsNew()->save();
      }
    }
    foreach ($taxonomy_term as $term) {
      $term_data = $this->cleanTerm($term);

      if (!empty($term_data['parents'])) {
        $parent_tid = $this->getTermIdByName($taxonomy_vocabulary, $term_data['parents']);
        $tid = $this->getTermIdByName($taxonomy_vocabulary, $term_data['name']);
        $term = Term::load($tid);
        $term->parent->setValue($parent_tid);
        $term->Save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function cleanTerm($data) {
    foreach ($data as $key => $value) {
      $term_data[trim($key)] = trim($value);
    }
    return $term_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabulary() {
    $vocabularies = Vocabulary::loadMultiple();
    $vocabulariesList = [];
    foreach ($vocabularies as $vid => $vocablary) {
      $vocabulariesList[$vid] = $vocablary->get('name');
    }
    return $vocabulariesList;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareTerms($vocablary, $term) {
    $prepareTerms = [];
    foreach ($term as $key => $value) {
      if ($key === 'parent') {
        $prepareTerms[$key] = $this->getTermIdByName($vocablary, $value);
      }
      else {
        $prepareTerms[$key] = $value;
      }
    }
    return $prepareTerms;
  }

  /**
   * {@inheritdoc}
   */
  public function getTermIdByName($vocablary, $term_name) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vocablary);
    $query->condition('name', $term_name);
    foreach ($query->execute() as $tid) {
      return $tid;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function csvtoarray($filename, $delimiter) {

    if (!file_exists($filename) || !is_readable($filename)) {
      return FALSE;
    }
    $header = NULL;
    $data = [];

    if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
        if (!$header) {
          $header = $row;
        }
        else {
          $data[] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }
    return $data;
  }

}
